
%%%
% 31/10/16: simplified figures for Thesis slides


FONT_SIZE = 20; % 12

% % H2_open ~ edge_diff/n_edges
% dblp_x = [ 94508.05/1049866 179795.05/1049866 246155.55/1049866 299586.05/1049866 344049.2/1049866 381093.05/1049866 413618.2/1049866 440956.2/1049866 466107.0/1049866 487283.1/1049866];
% dblp_H2 = [ 3257.2 744.0 325.7 199.2 140.7 109.8 86.6 77.6 65.6 60.0];
% 
% amazon_x = [ 104800.95/925872 196558.4/925872 266603.7/925872 322111.75/925872 367317.75/925872 405428.15/925872 437417.65/925872 465411.35/925872 489478.55/925872 510872.15/925872];
% amazon_H2 = [2209.1 452.4 188.4 118.8 82.4 67.3 51.1 44.4 38.7 35.2];
% 
% youtube_x = [ 213097.3/2987624 393235.9/2987624 521709.9/2987624 621156.2/2987624 697970.15/2987624 761248.25/2987624 814518.5/2987624 860208.1/2987624 900201.35/2987624 935484.2/2987624 ];
% youtube_H2 = [4428.8 1419.2 814.4 595.5 513.7 425.8 378.3 350.4 328.9 298.4];
% 
% % semilogy(dblp_x, dblp_H2, '-o', amazon_x, amazon_H2, '-+', youtube_x, youtube_H2, '-s');
% % ylhand = get(gca,'ylabel'); set(ylhand,'string','H2_{open} score (log scale)','fontsize',FONT_SIZE)
% plot(dblp_x, dblp_H2, '-o', amazon_x, amazon_H2, '-+', youtube_x, youtube_H2, '-s', 'LineWidth', 1.0);
% ylhand = get(gca,'ylabel'); set(ylhand,'string','H2_{open} score','fontsize',FONT_SIZE)
% set(gca,'YTick',[0:1000:4000]) 
% h_legend = legend('dblp','amazon','youtube');
% set(h_legend,'FontSize',FONT_SIZE);
% set(gca,'FontSize',FONT_SIZE)
% xlhand = get(gca,'xlabel'); set(xlhand,'string','ratio of replaced edges','fontsize',FONT_SIZE) 
% 
% 
% 
% % H1 ~ edge_diff/n_edges
% dblp_H1 = [ 59.7 40.7 32.1 29.5 27.0 24.4 22.5 21.3 19.7 19.5];
% 
% amazon_H1 = [30.2 22.8 17.8 17.2 15.2 14.8 13.5 13.6 12.4 12.6];
% 
% youtube_H1 = [114.4 84.2 71.4 65.3 62.8 58.6 57.7 56.7 54.0 52.1];
% 
% figure;
% plot(dblp_x, dblp_H1, '-o', amazon_x, amazon_H1, '-+', youtube_x, youtube_H1, '-s', 'LineWidth', 1.0);
% h_legend = legend('dblp','amazon','youtube');
% set(h_legend,'FontSize',FONT_SIZE);
% set(gca,'FontSize',FONT_SIZE)
% xlhand = get(gca,'xlabel'); set(xlhand,'string','ratio of replaced edges','fontsize',FONT_SIZE) 
% ylhand = get(gca,'ylabel'); set(ylhand,'string','H1 score','fontsize',FONT_SIZE)
% 
% % rel.err ~ edge_diff/n_edges
% dblp_err = [0.017 0.030 0.045 0.056 0.064 0.073 0.082 0.088 0.090 0.098];
% 
% amazon_err = [0.022 0.050 0.066 0.087 0.105 0.109 0.127 0.135 0.141 0.153];
% 
% youtube_err = [0.030 0.042 0.049 0.056 0.062 0.064 0.072 0.077 0.078 0.082];
% 
% figure;
% plot(dblp_x, dblp_err, '-o', amazon_x, amazon_err, '-+', youtube_x, youtube_err, '-s', 'LineWidth', 1.0);
% h_legend = legend('dblp','amazon','youtube', 'Location', 'NorthWest');
% set(h_legend,'FontSize',FONT_SIZE);
% set(gca,'FontSize',FONT_SIZE)
% xlhand = get(gca,'xlabel'); set(xlhand,'string','ratio of replaced edges','fontsize',FONT_SIZE) 
% ylhand = get(gca,'ylabel'); set(ylhand,'string','rel.err','fontsize',FONT_SIZE)
% set(gca,'YTick',[0.0:0.02:0.1]) 
% 
% 
% % runtime
% figure;
% partition_time = [1.6 1.6 1.6 1.6 1.6 1.3 1.3 1.3 1.3 1.3 9.9 9.9 9.9 9.9 9.9]';
% mps_time = [29.84 35.55 40.67 45.28 50.39 27.96 32.43 37.28 41.75 46.05 88.37 113.97 139.23 162.66 185.60]';
% solve_time = [23.25 30.08 37.00 45.45 56.10 23.88 33.71 43.72 53.21 65.77 55.75 66.49 75.44 87.36 99.60]';
% combine_time = [80.38 96.72 113.33 130.21 149.39 77.31 96.62 114.85 132.44 153.22 224.39 275.55 318.10 370.62 415.91]';
% 
% bar(1:15, [partition_time mps_time solve_time combine_time], 0.5, 'stack');
% h_legend = legend('partition','prepare subproblems','solve','combine', 'Location', 'NorthWest');
% set(h_legend,'FontSize',FONT_SIZE);
% xlhand = get(gca,'xlabel'); set(xlhand,'string','           dblp (1-5)      amazon (6-10)   youtube (11-15)','fontsize',FONT_SIZE) 
% ylhand = get(gca,'ylabel'); set(ylhand,'string','runtime (sec)','fontsize',FONT_SIZE)


%%%% tradeoff (privacy/utility)

% % DBLP
% figure;
% 
% ke_obf_PRIV = sqrt([40712.1 24618.2 7771.4]);
% ke_obf_UTIL = [0.018 0.077 0.128];
% 
% mv_PRIV = sqrt([60.0 65.6 77.6 86.6 109.8 140.7 199.2 325.7 744.0 3257.2]);
% mv_UTIL = [0.098 0.090 0.088 0.082 0.073 0.064 0.056 0.045 0.030 0.017];
% 
% rw_PRIV = sqrt([4.9 10.9 5.6 2.9]);
% rw_UTIL = [0.094 0.110 0.142 0.171];
% 
% rw_mod_PRIV = sqrt([4.5 9.4 5.4 2.6]);
% rw_mod_UTIL = [0.109 0.099 0.131 0.164];
% 
% mix_2_PRIV = sqrt([129.10 21.03 7.49]);
% mix_2_UTIL = [0.054 0.093 0.112];
% 
% mix_3_PRIV = sqrt([142.26 25.29 12.38]);
% mix_3_UTIL = [0.052 0.083 0.098];
% 
% mix_5_PRIV = sqrt([129.60 20.88 7.78]);
% mix_5_UTIL = [0.051 0.089 0.122];
% 
% 
% % plot(ke_obf_UTIL, ke_obf_PRIV, '-o', mv_UTIL, mv_PRIV, '-s', rw_UTIL, rw_PRIV, '-+')
% 
% % mix_2_UTIL, mix_2_PRIV, '-x', mix_3_UTIL, mix_3_PRIV, '-x',
% loglog(ke_obf_UTIL, ke_obf_PRIV, '-o', mv_UTIL, mv_PRIV, '-s', rw_UTIL, rw_PRIV, '-+', rw_mod_UTIL, rw_mod_PRIV, '-d', mix_5_UTIL, mix_5_PRIV, '-x', 'LineWidth', 1.0)
% axis([0.01 0.5 1 300]);
% h_legend = legend('(k,\epsilon)-obf','MV','RW','RW-mod','Mix,t=5');
% set(h_legend,'FontSize',FONT_SIZE);
% set(gca,'FontSize',FONT_SIZE)
% xlhand = get(gca,'xlabel'); set(xlhand,'string','Relative error (log scale)','fontsize',FONT_SIZE) 
% ylhand = get(gca,'ylabel'); set(ylhand,'string','{\surd}H2_{open} (log scale)','fontsize',FONT_SIZE)


% AMAZON
% figure;
% 
% ke_obf_PRIV = sqrt([55655.9 39689.8 16375.4]);
% ke_obf_UTIL = [0.057 0.107 0.144];
% 
% mv_PRIV = sqrt([35.2 38.7 44.4 51.1 67.3 82.4 118.8 188.4 452.4 2209.1]);       % H2_open
% mv_UTIL = [0.153 0.141 0.135 0.127 0.109 0.105 0.087 0.066 0.050 0.022];
% 
% rw_PRIV = sqrt([5.4 16.5 8.6 4.6]);
% rw_UTIL = [0.180 0.137 0.181 0.234];
% 
% rw_mod_PRIV = sqrt([3.2 11.2 6.0 3.3]);
% rw_mod_UTIL = [0.139 0.134 0.185 0.238];
% 
% mix_2_PRIV = sqrt([]);
% mix_2_UTIL = [];
% 
% mix_3_PRIV = sqrt([]);
% mix_3_UTIL = [];
% 
% mix_5_PRIV = sqrt([124.63 20.55 7.97]);
% mix_5_UTIL = [0.060 0.128 0.168];
% 
% 
% % plot(ke_obf_UTIL, ke_obf_PRIV, '-o', mv_UTIL, mv_PRIV, '-s', rw_UTIL, rw_PRIV, '-+')
% 
% % mix_2_UTIL, mix_2_PRIV, '-x', mix_3_UTIL, mix_3_PRIV, '-x',
% loglog(ke_obf_UTIL, ke_obf_PRIV, '-o', mv_UTIL, mv_PRIV, '-s', rw_UTIL, rw_PRIV, '-+', rw_mod_UTIL, rw_mod_PRIV, '-d', mix_5_UTIL, mix_5_PRIV, '-x', 'LineWidth', 1.0)
% axis([0.02 0.5 1 300]);
% h_legend = legend('(k,\epsilon)-obf','MV','RW','RW-mod','Mix,t=5');
% set(h_legend,'FontSize',FONT_SIZE);
% set(gca,'FontSize',FONT_SIZE)
% xlhand = get(gca,'xlabel'); set(xlhand,'string','Relative error (log scale)','fontsize',FONT_SIZE) 
% ylhand = get(gca,'ylabel'); set(ylhand,'string','{\surd}H2_{open} (log scale)','fontsize',FONT_SIZE)


% YOUTUBE
% figure;
% 
% ke_obf_PRIV = sqrt([36744.6 22361.7 5806.9]);
% ke_obf_UTIL = [0.022 0.043 0.160];
% 
% mv_PRIV = sqrt([298.4 328.9 350.4 378.3 425.8 513.7 595.5 814.4 1419.2 4428.8]);
% mv_UTIL = [0.082 0.078 0.077 0.072 0.064 0.062 0.056 0.049 0.042 0.030];
% 
% rw_PRIV = sqrt([1.5 17.6 8.4 1.8]);
% rw_UTIL = [0.403 0.103 0.120 0.145];
% 
% rw_mod_PRIV = sqrt([1.4 22.3 11.0 1.7]);
% rw_mod_UTIL = [0.245 0.081 0.090 0.099];
% 
% mix_2_PRIV = sqrt([]);
% mix_2_UTIL = [];
% 
% mix_3_PRIV = sqrt([]);
% mix_3_UTIL = [];
% 
% mix_5_PRIV = sqrt([65.41 10.88 4.33]);
% mix_5_UTIL = [0.047 0.080 0.086];
% 
% 
% % plot(ke_obf_UTIL, ke_obf_PRIV, '-o', mv_UTIL, mv_PRIV, '-s', rw_UTIL, rw_PRIV, '-+')
% 
% % mix_2_UTIL, mix_2_PRIV, '-x', mix_3_UTIL, mix_3_PRIV, '-x',
% loglog(ke_obf_UTIL, ke_obf_PRIV, '-o', mv_UTIL, mv_PRIV, '-s', rw_UTIL, rw_PRIV, '-+', rw_mod_UTIL, rw_mod_PRIV, '-d', mix_5_UTIL, mix_5_PRIV, '-x', 'LineWidth', 1.0)
% axis([0.02 1.0 1 300]);
% h_legend = legend('(k,\epsilon)-obf','MV','RW','RW-mod','Mix,t=5');
% set(h_legend,'FontSize',FONT_SIZE);
% set(gca,'FontSize',FONT_SIZE)
% xlhand = get(gca,'xlabel'); set(xlhand,'string','Relative error (log scale)','fontsize',FONT_SIZE) 
% ylhand = get(gca,'ylabel'); set(ylhand,'string','{\surd}H2_{open} (log scale)','fontsize',FONT_SIZE)


%%%% total variance

% % DBLP (t=2,3,5)
% n_edges_DB = 1049866;
% 
% rw_mod_TV_DB = [619319.5 720500.6 855358.3];
% n_nonzero_DB = [27608710 333205515 16239333864];
% rw_mod_TV_DB_upper = n_edges_DB*(n_nonzero_DB-n_edges_DB)./n_nonzero_DB;
% 
% ke_obf_TV_DB = [5088.5 13302.9 38569.1];
% 
% mv_TV_DB = [94740.3 180018.3 246658.8 299910.2 344385.9 381549.8 414244.9 441693.0 466489.1 487838.3];
% n_new_edges_DB = 1000 * [200 400 600 800 1000 1200 1400 1600 1800 2000];
% mv_TV_upper_DB = n_edges_DB*n_new_edges_DB./(n_edges_DB + n_new_edges_DB);
% 
% % AMAZON (t=2,3,5)
% n_edges_AM = 925872;
% 
% rw_mod_TV_AM = [521821.7 609362.7 718280.5];
% n_nonzero_AM = [13779633 51054358 672739488];
% rw_mod_TV_AM_upper = n_edges_AM*(n_nonzero_AM-n_edges_AM)./n_nonzero_AM;
% 
% ke_obf_TV_AM = [4398.4 9333.5 23187.0];
% 
% mv_TV_AM = [104967.4 196749.4 266791.3 322213.7 367582.1 405570.8 437553.2 465648.6 489809.1 510925.2];
% n_new_edges_AM = 1000 * [200 400 600 800 1000 1200 1400 1600 1800 2000];
% mv_TV_upper_AM = n_edges_AM*n_new_edges_AM./(n_edges_AM + n_new_edges_AM);
% 
% %
% DB = [ke_obf_TV_DB([1 2 3]) mv_TV_DB([2 4 6 8 10]) rw_mod_TV_DB];
% AM = [ke_obf_TV_AM([1 2 3]) mv_TV_AM([2 4 6 8 10]) rw_mod_TV_AM];
% 
% b = bar(1:11, [DB'/n_edges_DB AM'/n_edges_AM], 'BarWidth', 0.5);
% set(b(2),'FaceColor','white');
% % hold on; line([1 11],[n_edges_DB n_edges_DB], 'Color', 'b')
% % line([1 11],[n_edges_AM n_edges_AM], 'Color', 'g')
% % bar(9:16, AM, 0.5, 'stack')
% h_legend = legend('dblp','amazon', 'Location', 'NorthWest');
% set(h_legend,'FontSize',FONT_SIZE);
% xlhand = get(gca,'xlabel'); set(xlhand,'string','k-\epsilon(1-3)                 MaxVar (4-8)            RW-mod (9-11)','fontsize',FONT_SIZE) 
% ylhand = get(gca,'ylabel'); set(ylhand,'string','Total variance/number of edges','fontsize',FONT_SIZE)


% labels = cellstr( ['1';'2';'3';'4';'5';'6';'7';'8']);  %' # labels correspond to their order
% 
% plot(1:8, DB, 'o')
% text(1:8, DB, labels, 'VerticalAlignment','bottom', ...
%                              'HorizontalAlignment','right')








%%%%%%%%%%%%%%%%% 31/10/2016, simplified figures for Thesis slides
%%%% tradeoff (privacy/utility)

% DBLP
figure;

ke_obf_PRIV = sqrt([40712.1 24618.2 7771.4]);
ke_obf_UTIL = [0.018 0.077 0.128];

mv_PRIV = sqrt([60.0 65.6 77.6 86.6 109.8 140.7 199.2 325.7 744.0 3257.2]);
mv_UTIL = [0.098 0.090 0.088 0.082 0.073 0.064 0.056 0.045 0.030 0.017];

rw_PRIV = sqrt([4.9 10.9 5.6 2.9]);
rw_UTIL = [0.094 0.110 0.142 0.171];

rw_mod_PRIV = sqrt([4.5 9.4 5.4 2.6]);
rw_mod_UTIL = [0.109 0.099 0.131 0.164];

mix_2_PRIV = sqrt([129.10 21.03 7.49]);
mix_2_UTIL = [0.054 0.093 0.112];

mix_3_PRIV = sqrt([142.26 25.29 12.38]);
mix_3_UTIL = [0.052 0.083 0.098];

mix_5_PRIV = sqrt([129.60 20.88 7.78]);
mix_5_UTIL = [0.051 0.089 0.122];


% plot(ke_obf_UTIL, ke_obf_PRIV, '-o', mv_UTIL, mv_PRIV, '-s', rw_UTIL, rw_PRIV, '-+')

% mix_2_UTIL, mix_2_PRIV, '-x', mix_3_UTIL, mix_3_PRIV, '-x',
loglog(ke_obf_UTIL, ke_obf_PRIV, '-o', mv_UTIL, mv_PRIV, '-s', rw_UTIL, rw_PRIV, '-+')
axis([0.01 0.5 1 300]);
h_legend = legend('(k,\epsilon)-obf','MaxVar','RandWalk');
set(h_legend,'FontSize',FONT_SIZE);
set(gca,'FontSize',FONT_SIZE)
xlhand = get(gca,'xlabel'); set(xlhand,'string','Relative error (log scale)','fontsize',FONT_SIZE) 
ylhand = get(gca,'ylabel'); set(ylhand,'string','{\surd}H2_{open} (log scale)','fontsize',FONT_SIZE)
saveas(gcf, 'eps/tradeoff-dblp.eps', 'epsc')

% AMAZON
figure;

ke_obf_PRIV = sqrt([55655.9 39689.8 16375.4]);
ke_obf_UTIL = [0.057 0.107 0.144];

mv_PRIV = sqrt([35.2 38.7 44.4 51.1 67.3 82.4 118.8 188.4 452.4 2209.1]);       % H2_open
mv_UTIL = [0.153 0.141 0.135 0.127 0.109 0.105 0.087 0.066 0.050 0.022];

rw_PRIV = sqrt([5.4 16.5 8.6 4.6]);
rw_UTIL = [0.180 0.137 0.181 0.234];

rw_mod_PRIV = sqrt([3.2 11.2 6.0 3.3]);
rw_mod_UTIL = [0.139 0.134 0.185 0.238];

mix_2_PRIV = sqrt([]);
mix_2_UTIL = [];

mix_3_PRIV = sqrt([]);
mix_3_UTIL = [];

mix_5_PRIV = sqrt([124.63 20.55 7.97]);
mix_5_UTIL = [0.060 0.128 0.168];


% plot(ke_obf_UTIL, ke_obf_PRIV, '-o', mv_UTIL, mv_PRIV, '-s', rw_UTIL, rw_PRIV, '-+')

% mix_2_UTIL, mix_2_PRIV, '-x', mix_3_UTIL, mix_3_PRIV, '-x',
loglog(ke_obf_UTIL, ke_obf_PRIV, '-o', mv_UTIL, mv_PRIV, '-s', rw_UTIL, rw_PRIV, '-+')
axis([0.02 0.5 1 300]);
h_legend = legend('(k,\epsilon)-obf','MaxVar','RandWalk');
set(h_legend,'FontSize',FONT_SIZE);
set(gca,'FontSize',FONT_SIZE)
xlhand = get(gca,'xlabel'); set(xlhand,'string','Relative error (log scale)','fontsize',FONT_SIZE) 
ylhand = get(gca,'ylabel'); set(ylhand,'string','{\surd}H2_{open} (log scale)','fontsize',FONT_SIZE)
saveas(gcf, 'eps/tradeoff-amazon.eps', 'epsc')

% YOUTUBE
figure;

ke_obf_PRIV = sqrt([36744.6 22361.7 5806.9]);
ke_obf_UTIL = [0.022 0.043 0.160];

mv_PRIV = sqrt([298.4 328.9 350.4 378.3 425.8 513.7 595.5 814.4 1419.2 4428.8]);
mv_UTIL = [0.082 0.078 0.077 0.072 0.064 0.062 0.056 0.049 0.042 0.030];

rw_PRIV = sqrt([1.5 17.6 8.4 1.8]);
rw_UTIL = [0.403 0.103 0.120 0.145];

rw_mod_PRIV = sqrt([1.4 22.3 11.0 1.7]);
rw_mod_UTIL = [0.245 0.081 0.090 0.099];

mix_2_PRIV = sqrt([]);
mix_2_UTIL = [];

mix_3_PRIV = sqrt([]);
mix_3_UTIL = [];

mix_5_PRIV = sqrt([65.41 10.88 4.33]);
mix_5_UTIL = [0.047 0.080 0.086];


% plot(ke_obf_UTIL, ke_obf_PRIV, '-o', mv_UTIL, mv_PRIV, '-s', rw_UTIL, rw_PRIV, '-+')

% mix_2_UTIL, mix_2_PRIV, '-x', mix_3_UTIL, mix_3_PRIV, '-x',
loglog(ke_obf_UTIL, ke_obf_PRIV, '-o', mv_UTIL, mv_PRIV, '-s', rw_UTIL, rw_PRIV, '-+')
axis([0.02 1.0 1 300]);
h_legend = legend('(k,\epsilon)-obf','MaxVar','RandWalk');
set(h_legend,'FontSize',FONT_SIZE);
set(gca,'FontSize',FONT_SIZE)
xlhand = get(gca,'xlabel'); set(xlhand,'string','Relative error (log scale)','fontsize',FONT_SIZE) 
ylhand = get(gca,'ylabel'); set(ylhand,'string','{\surd}H2_{open} (log scale)','fontsize',FONT_SIZE)
saveas(gcf, 'eps/tradeoff-youtube.eps', 'epsc')





