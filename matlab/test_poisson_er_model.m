% Compute (theoretically) number of selfloops and multiedges of
% RandWalk-mod on Erdos-Renyi (ER) graphs

% lambda = average degree ~ 6
ld = 8;

n_nodes = 1000000;

% binary search for max_deg
lo = ceil(ld);
hi = n_nodes;
while 1
    if abs(lo-hi) <= 1
        break
    end
    
%     fprintf('lo = %d  hi = %d\n', lo, hi);
    
    k = floor((lo + hi)/2);
    if k*(log(k)-1-log(ld)) + 0.5*log(2*pi*k) < log(n_nodes) - ld
        lo = k;
    else
        hi = k-1;
    end
end

max_deg = hi;

deg_list = [1:max_deg];
p = ones(1,max_deg);
for k=1:max_deg
    for i=1:k
        p(k) = p(k)*ld/i;
    end
    p(k) = p(k)*exp(-ld);
end

% normalize
p = p/sum(p);

deg_count = n_nodes * p;                    
n_edges = sum(deg_count.*[1:max_deg])/2;
avg_deg_real = n_edges*2/n_nodes;

avg_deg_limit = ld;        % mean of Poisson distribution

% number of selfloops
n_selfloops = sum(deg_list.*deg_list.*deg_count)/(2*n_edges);

n_selfloops_limit = ld + 1;

% number of multiedges
n_multiedges = 0;
for i=1:length(deg_list)
    for j=i+1:length(deg_list)
        if deg_list(i)*deg_list(j) > 2*n_edges
            n_multiedges = n_multiedges + 2*deg_count(i)*deg_count(j)*floor(deg_list(i)*deg_list(j)/(2*n_edges));     % factor 2 for symmetric (i,j) (j,i)
        end
    end
end

% total variance limit (t=inf)
total_var = 0;
for i=1:length(deg_list)
    for j=1:length(deg_list)
        total_var = total_var + 0.5*deg_list(i)^2*deg_list(j)^2*deg_count(i)*deg_count(j);
    end
end
total_var = n_edges - total_var/(8*n_edges^2);

total_var_limit = n_edges - (ld+1)^2;






