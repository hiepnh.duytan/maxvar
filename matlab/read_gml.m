function [ edge_list, A ] = read_gml(fileName)
%Extracting edges from gml file graph

    inputfile = fopen(fileName);
    
    % read edge list
    edge_list=[];
    l=0;
    k=1;
    while 1
        % Get a line from the input file
        tline = fgetl(inputfile);
        % Quit if end of file
        if ~ischar(tline)
            break
        end        
        nums = regexp(tline,'\d+','match');
        if length(nums)
            if l==1
                l=0;
                edge_list(k,2)=str2num(nums{1});  
                k=k+1;
                continue;
            end
            edge_list(k,1)=str2num(nums{1});
            l=1;
        else
            l=0;
            continue;
        end
    end

    % build adjacency matrix
    if min(min(edge_list)) == 0
        dt = 1;
    else
        dt = 0;
    end
    N = max(max(edge_list)) + dt;
    A = zeros(N, N);
    for k=1:length(edge_list)
        i = edge_list(k,1) + dt;
        j = edge_list(k,2) + dt;
        A(i,j) = 1;
        A(j,i) = 1;
    end
    
end

