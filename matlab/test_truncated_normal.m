
% truncated normal on [0,1]

% constant C
sigma = 1;    % 1.0 0.5 0.1, 0.01, 0.001

C = 1/(0.5*erf(1/(sigma*sqrt(2))));

step = 0.000001;
X = [0:step:1];
Y = normpdf(X,0,sigma);    % pdf values on [0,1]
I_simul = sum(Y)*step;              % integral of f(x) on [0,1]
C_simul = 1/I_simul;

% mean E[r_e]
E_re = C*sigma/sqrt(2*pi)*(1-exp(-1/(2*sigma^2)));

E_re_simul = C_simul*sum(X.*Y)*step;


% second moment E[r_e]
E2_re = C*sigma/sqrt(2*pi)*(-exp(-1/(2*sigma^2)) + sqrt(2*pi)*sigma/C);

E2_re_simul = C_simul*sum(X.*X.*Y)*step;