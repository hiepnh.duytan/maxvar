% Compute the number of selfloops and multiedges of RandWalk-mod on real
% datasets

% load 'C:\com_dblp_ungraph__deg_list'
% load 'C:\com_amazon_ungraph__deg_list'
load 'C:\com_youtube_ungraph__deg_list'

unv = unique(deg_list);
a = [unv histc(deg_list,unv)];

max_deg = max(a(:,1));
n_edges = sum(deg_list)/2;
disp('n_edges = ')
disp(n_edges)

disp('check multiedges')
disp(max_deg^2/(2*n_edges))

n_selfloops = sum(a(:,1).*a(:,1).*a(:,2))/(2*n_edges);
disp('n_selfloops = ')
disp(n_selfloops)

% 
n_multiedges = 0;
for i=1:length(a(:,1))
    for j=i+1:length(a(:,1))
        if a(i,1)*a(j,1) > 2*n_edges
            n_multiedges = n_multiedges + 2*a(i,2)*a(j,2)*floor(a(i,1)*a(j,1)/(2*n_edges));     % factor 2 for symmetric (i,j) (j,i)
        end
    end
end

disp('n_multiedges = ')
disp(n_multiedges)