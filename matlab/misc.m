% draw normal distribution

% x = -2:0.01:2;
% sigma1 = 0.1;
% sigma2 = 0.5;
% sigma3 = 1.0;
% y1 = 1/sqrt(2*pi)/sigma1 * exp(-x.^2/(2*sigma1^2));
% y2 = 1/sqrt(2*pi)/sigma2 * exp(-x.^2/(2*sigma2^2));
% y3 = 1/sqrt(2*pi)/sigma3 * exp(-x.^2/(2*sigma3^2));
% 
% plot(x,y1,'-k',x,y2,'-r',x,y3,'--b'); axis equal;
% 
% axis([-2 2 -0.1 4])
% 
% line([-2 2],[0 0]); line([0 0],[-0.1 4]); line([1 1],[-0.1 4]); 
% 
% h_legend = legend('\sigma=0.1','\sigma=0.5','\sigma=1.0');
% set(h_legend,'FontSize',16);

%
x = -2:0.01:2;
x1 = -2:0.01:0;
x2 = 0:0.01:1;
x3 = 1:0.01:2;
sigma1 = 0.1;
sigma2 = 0.5;
sigma3 = 1.0;
y1_1 = 1/sqrt(2*pi)/sigma1 * exp(-x1.^2/(2*sigma1^2));
y1_2 = 1/sqrt(2*pi)/sigma1 * exp(-x2.^2/(2*sigma1^2));
y1_3 = 1/sqrt(2*pi)/sigma1 * exp(-x3.^2/(2*sigma1^2));

y2_1 = 1/sqrt(2*pi)/sigma2 * exp(-x1.^2/(2*sigma2^2));
y2_2 = 1/sqrt(2*pi)/sigma2 * exp(-x2.^2/(2*sigma2^2));
y2_3 = 1/sqrt(2*pi)/sigma2 * exp(-x3.^2/(2*sigma2^2));

y3_1 = 1/sqrt(2*pi)/sigma3 * exp(-x1.^2/(2*sigma3^2));
y3_2 = 1/sqrt(2*pi)/sigma3 * exp(-x2.^2/(2*sigma3^2));
y3_3 = 1/sqrt(2*pi)/sigma3 * exp(-x3.^2/(2*sigma3^2));

plot(x2,y1_2,'-k',x2,y2_2,'-r',x2,y3_2,'-g',x1,y1_1,'--k',x1,y2_1,'--r',x1,y3_1,'--g',x3,y1_3,'--k',x3,y2_3,'--r',x3,y3_3,'--g'); axis equal;
% then use Edit Plot to change line widths to 2

axis([-2 2 -0.1 4])

line([-2 2],[0 0]); line([0 0],[-0.1 4]); line([1 1],[-0.1 4]); 

h_legend = legend('\sigma=0.1','\sigma=0.5','\sigma=1.0');
set(h_legend,'FontSize',16);


