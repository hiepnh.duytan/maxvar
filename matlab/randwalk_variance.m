
T = 10;
n_nodes = size(A,1);

P_rw = zeros(n_nodes);
for i=1:n_nodes
    for j=1:n_nodes
        if A(i,j) == 1
            P_rw(i,j) = 1/sum(A(i,:));
        end
    end
end

for t=2:T
    B = A*P_rw^(t-1);
    var = sum(sum(B.*(1-B)));

    total_var = 0;
    total_selfloop = sum(diag(B));
    total_multiedge = 0;
    for i=1:n_nodes
        for j=1:n_nodes
            p = B(i,j) - floor(B(i,j));         % only get the fraction
            total_var = total_var + p*(1-p);    
            if i ~= j % && B(i,j) > 1
                total_multiedge = total_multiedge + floor(2*B(i,j));  % excluding all selfloops
            end
        end
    end
    fprintf('t=%d, var = %f, total_var = %f, total_selfloop = %f, total_multiedge = %f\n', t, var, total_var, total_selfloop, total_multiedge);    % need to divide by 2
    
end