
eps = 1.0;
mu = 20;
sigma = sqrt(mu/2);

%%% EXP-NORMAL
% maxY = exp(eps*mu - eps^2*sigma^2);
% y =0.01:10:(20*maxY);
% y =0.01:10:(200*maxY);
% 
% fY = zeros(1,length(y));
% for idx=1:1:length(y)
%     fY(idx) = exp_normal(y(idx),eps,mu,sigma); 
% end

%%% EXP-NORMAL-INVERSE
maxY = exp(-eps*mu - eps^2*sigma^2);

y =0.0001:0.0001:1;
fY = zeros(1,length(y));
for idx=1:1:length(y)
%     fY(idx) = exp_normal(y(idx),eps,mu,sigma); 
    fY(idx) = exp_normal_inverse(y(idx),eps,mu,sigma);    
end