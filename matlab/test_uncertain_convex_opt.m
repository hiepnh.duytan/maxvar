

A = [1 0 0 0 0 0 1 1;
     1 1 1 0 0 1 0 0;
     0 1 0 1 0 0 0 1;
     0 0 1 1 1 0 1 0;
     0 0 0 0 1 1 0 0];
b = [1 3 2 3 1]';
A1 = A(:,1:5);
A2 = A(:,6:8);

inv_A1 = A1\eye(5);   %inv(A1);
Q = A2'*(inv_A1'*inv_A1)*A2 + eye(3);

p = b'*(inv_A1'*inv_A1)*A2;
p = p';

max_x = Q\p;

% test 
x1_val = inv_A1*(b-A2*max_x);

inv_A1_b = inv_A1*b;
inv_A1_A2 = inv_A1*A2;
