% Compute (theoretically) number of selfloops and multiedges of
% RandWalk-mod on Power-Law (PL) graphs

% gamma in interval (2,3)
gm = 2.24;   % 2.1 --> lower gm, more difference between avg_deg_real and avg_deg_limit

n_nodes = 1000000;
% n_nodes = 10000000;     % --> bigger n_nodes, less difference between avg_deg_real and avg_deg_limit
max_deg = floor((n_nodes/zeta(gm))^(1/gm));     % from the requirement: size of deg_i = n*i^(-gm)/zeta(gm) >= 1
% max_deg = 1000;

deg_list = [1:max_deg];
p = 1./deg_list.^gm;                        % size of deg_i

p = p/sum(p);

deg_count = n_nodes * p;                    
n_edges = sum(deg_count.*[1:max_deg])/2;
avg_deg_real = n_edges*2/n_nodes;

avg_deg_limit = zeta(gm-1)/zeta(gm);        % mean of zeta distribution

% number of selfloops
n_selfloops = sum(deg_list.*deg_list.*deg_count)/(2*n_edges);

n_selfloops_limit = -1;
if gm > 3
    n_selfloops_limit = zeta(gm-2)/zeta(gm-1);  % theoretical value
end

% number of multiedges
n_multiedges = 0;
for i=1:length(deg_list)
    for j=i+1:length(deg_list)
        if deg_list(i)*deg_list(j) > 2*n_edges
            n_multiedges = n_multiedges + 2*deg_count(i)*deg_count(j)*floor(deg_list(i)*deg_list(j)/(2*n_edges));     % factor 2 for symmetric (i,j) (j,i)
        end
    end
end

% total variance limit (t=inf)
total_var = 0;
for i=1:length(deg_list)
    for j=1:length(deg_list)
        total_var = total_var + deg_list(i)^2*deg_list(j)^2*deg_count(i)*deg_count(j);
    end
end
total_var = n_edges - total_var/(4*n_edges^2);

total_var_limit = -1;
if gm > 3
    total_var_limit = n_edges - (zeta(gm-2)/zeta(gm-1))^2;
end


