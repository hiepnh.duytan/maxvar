

%%% for ENTROPY-BASED
% % file_name = 'er_1000_005_en_0.1';
% % file_name = 'er_1000_005_en_0.5';
% file_name = 'er_1000_005_en_1.0';
% 
% a = load(strcat(file_name,'.out'));
% n_edges = 24912;
% 
% threshold = n_edges/size(a,1);
% 
% val = quantile(a(:,3),1-threshold);
% 
% idx = find(a(:,3) >= val);
% b=a(idx,:);
% dlmwrite(strcat(file_name,'_filtered.txt'),b,'precision',4,'delimiter','\t')


%%% for DIFFERENTIAL PRIVACY
% file_name = 'er_1000_005_dp2_0.1';
% file_name = 'er_1000_005_dp2_1.0';
% file_name = 'er_1000_005_dp2_10.0';
% file_name = 'er_1000_005_dp2_100.0';

file_name = 'er_10000_0005_dp2_1.0';

a = load(strcat(file_name,'.out'));
b = a(:,3);
[c,idx] = sort(b,'descend');

% n_edges = 24912;  % er_1000_005.gr
n_edges = 248793;   % er_10000_0005.gr 

b = a(idx(1:n_edges),:);

dlmwrite(strcat(file_name,'_filtered.txt'),b,'precision',4,'delimiter','\t')

