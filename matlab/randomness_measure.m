
[edge_list, A] = read_gml('C:/Tailieu/Paper-code/DATA-SET/Network data - Newman (Umich)/polbooks/polbooks-2.gml');

k = 2;
% top-k eigenvalues/eigenvectors
[v, D] = eigs(A,k);

plot(v(:,1), v(:,2),'+'); axis equal;
