function [fY] = exp_normal(y, eps, mu, sigma)
% for y > 0

fY = 1/(eps*y*sqrt(2*pi)*sigma) * exp(-(log(y)/eps - mu)^2/(2*sigma^2));


end

