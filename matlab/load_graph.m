
load 'c:/polbooks.mat'

N = max(max(edge_list)) + 1;
A = zeros(N, N);
for k=1:length(edge_list)
    i = edge_list(k,1) + 1;
    j = edge_list(k,2) + 1;
    A(i,j) = 1;
    A(j,i) = 1;
end

%

k = 2;
% top-k eigenvalues/eigenvectors
[v, D] = eigs(A,k);

id1 = find(label_list == 1);
id2 = find(label_list == 2);
id3 = find(label_list == 3);

v1 = v(id1,:);
v2 = v(id2,:);
v3 = v(id3,:);

plot(v1(:,1), v1(:,2),'r+', v2(:,1), v2(:,2),'bo', v3(:,1), v3(:,2),'k+'); axis equal;