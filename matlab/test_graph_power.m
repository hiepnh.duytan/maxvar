
% A = [0 1 0 1; 1 0 1 1; 0 1 0 0; 1 1 0 0];

% A = [0 1 0 1; 1 0 1 0; 0 1 0 1; 1 0 1 0];

% A = [0 1 1 1; 1 0 1 1; 1 1 0 1; 1 1 1 0];

% A = [0 1 0 0 1; 1 0 1 0 0; 0 1 0 1 0; 0 0 1 0 1; 1 0 0 1 0];    % 2-regular

% A = [0 1 1 1 0; 1 0 0 0 0; 1 0 0 1 1; 1 0 1 0 1; 0 0 1 1 0];

A = [0 1 1 1 0 0; 1 0 0 0 0 0; 1 0 0 1 1 1; 1 0 1 0 1 0; 0 0 1 1 0 0; 0 0 1 0 0 0];

t = 20;
S = zeros(1,t);
for i=1:t
    S(i) = sum(sum(A^i));
end

semilogy(1:t,S)
