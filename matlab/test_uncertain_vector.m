
% edge probabilitites
% p = [0.5 0.5 0.5];
% p = [0.5 0.3 0.7];
% p = [0.8 0.7 0.6];
% p = [0.8 0.3 0.6];
% p = [0.9 0.9 0.1];
% p = [0.9 0.1 0.1];
% p = [0.9 0.9 0.9];
% p = [1.0 1.0 0.0];
p = [0.8 0.3 0.6 0.5];

% p = [0.8];
% p = [0.8 0.3];

k = length(p);

vals = (2^k-1):-1:0;

probs = [p(1) 1-p(1)]';
for i=2:k
    probs = kron(probs, [p(i) 1-p(i)]');
end

%
mean_v = probs'*vals';
var_v = probs'*((vals-mean_v).^2)';

display(sum(p.^2))
display(var_v)
